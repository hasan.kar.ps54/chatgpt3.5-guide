# ChatGPT3.5 Guide



## Greetings

Уважаемые коллеги из 10 школы, рад приветствовать вас! 
В рамках данного туториала я представлю вам алгоритм подготовки ПК к использованию чудесного искусственного интеллекта ChatGPT, а также процесс авторизации на сайте OpenAI.


**P.S.** На этом этапе мы опустим детали покупки аккаунта, поскольку я предоставлю вам доступ к ним абсолютно бесплатно. 


**P.P.S.** Важно отметить, что, данные аккаунты подвержены риску быть забаненными по различным причинам или просто прекратить свою работу в какой-то момент.

## Step 1

Туториал предусматривает использование браузера Google Chrome.
Для использования ChatGPT обязательно необходимо пользоваться сервисами VPN для доступа из РФ.


По данной ссылке [КЛИК](https://chromewebstore.google.com/detail/hola-vpn-the-website-unbl/gkojfkhlekighikafcpjkiklfbnlmeio) устанавливаем VPN расширение для Google Chrome.

У меня уже настроено расширение. В вашем случае, здесь будет кнопка 'Установить', чтобы запустить инсталяцию.
![Установка Hola VPN](/picture/1.png "Установка Hola VPN")

## Step 2

После устаровки расширение появится в меню быстрого доступа Chrome.
1. Расширения
2. Hola VPN
![](/picture/2.png)
3. Click to connect
![](/picture/3.png)
**GOTCHA!**
VPN успешно активирован для ***текущей*** страницы.
## Step 3

Подготовка ПК завершена.

Далее переходим на сайт [OpenAI](https://auth.openai.com/authorize?client_id=TdJIcbe16WoTHtN95nyywh5E4yOo6ItG&scope=openid+email+profile+offline_access+model.request+model.read+organization.read+organization.write&response_type=code&redirect_uri=https%3A%2F%2Fchat.openai.com%2Fapi%2Fauth%2Fcallback%2Flogin-web&audience=https%3A%2F%2Fapi.openai.com%2Fv1&device_id=913b8c0e-2cb9-4031-8f45-3c676f27d6ad&prompt=login&state=iVABpMAKvohu389FABnW0Axrkg9FslE6LVEWiAFb5TA&code_challenge=ZdWqHVKc-SDV8ch6jk9MS36FcYgLfbP13gcAXC5OpsQ&code_challenge_method=S256) со включенным VPN для страницы.


### Вводим **почту**:
![](/picture/4.png)

### Вводим **пароль**:
![](/picture/5.png)
### Используем результаты технологического прогресса.
![](/picture/6.png)
## Ending
### Добро пожаловать в сообщество лентев!
<img src="/picture/7.gif" width="800" height="400">